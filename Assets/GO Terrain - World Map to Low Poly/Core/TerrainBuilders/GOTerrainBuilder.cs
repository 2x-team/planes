using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using GoShared;

namespace GoTerrain {


	public class GOTerrainBuilder : MonoBehaviour {

		#region TERRAIN TILE

		public static void BuildTerrain (GOTerrainTile parentTile, Texture2D tex, Texture2D normalTex, GOTerrainOptions options) {


			GameObject tileObj = parentTile.gameObject;

			tileObj.AddComponent <MeshRenderer> ().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
		
			tileObj.AddComponent<MeshFilter> ().mesh = InitTerrain (tex,normalTex, options, parentTile, Vector2.zero, new Vector2 (options.resolution,options.resolution) );

			MeshCollider meshCol = tileObj.GetComponent<MeshCollider> ();

			if (meshCol) {
				tileObj.GetComponent <MeshCollider> ().sharedMesh = null;
				tileObj.GetComponent <MeshCollider> ().sharedMesh = tileObj.GetComponent<MeshFilter>().sharedMesh;
			} else {
				tileObj.AddComponent <MeshCollider> ().sharedMesh = tileObj.GetComponent<MeshFilter>().sharedMesh;
			}
		}
			
		public static void ColorTerrainTile (GameObject parentTile, Texture2D tex, Texture2D normalTex , GOTerrainOptions options, float tileWidth) {

			MeshRenderer renderer = parentTile.GetComponent<MeshRenderer> ();
			GOTerrainTile tile = parentTile.GetComponent<GOTerrainTile>();
			MeshFilter filter;

			if (options.generationMode == GOTerrainOptions.TerrainGenerationMode.GradientColor) {
				if (options.gradientOptions.useLowPoly)
					renderer.sharedMaterial = options.baseMaterials.lowPolyMaterial;
				else
					renderer.sharedMaterial = options.baseMaterials.gradientMaterial;
				filter = tile.GetComponent<MeshFilter> ();
				filter.sharedMesh = GOTerrainBuilder.ColorTerrain (tex, options, Vector2.zero, new Vector2 (options.resolution, options.resolution), filter.sharedMesh);
			} else if (options.generationMode == GOTerrainOptions.TerrainGenerationMode.TerrainImagery) {
				switch (options.imageryOptions.imageMode) {
//				case GOTerrainImageryOptions.TerrainImageryMode.Color:
//					renderer.sharedMaterial = options.baseMaterials.material;
//					filter = tile.GetComponent<MeshFilter> ();
//					filter.sharedMesh = GOTerrainBuilder.ColorTerrain (tex, options, Vector2.zero, new Vector2 (options.resolution,options.resolution) ,filter.sharedMesh);
//					break;

				case GOTerrainImageryOptions.TerrainImageryMode.LowPoly_VertexColor:
					renderer.sharedMaterial = options.baseMaterials.lowPolyMaterial;
					filter = tile.GetComponent<MeshFilter> ();
					filter.sharedMesh = GOTerrainBuilder.ColorTerrain (tex, options, Vector2.zero, new Vector2 (options.resolution,options.resolution) ,filter.sharedMesh);
					break;

				case GOTerrainImageryOptions.TerrainImageryMode.Texture:
					
					Material baseMat = new Material( options.baseMaterials.terrainImagegeryMaterial);

					if (options.useNormals) {
						baseMat.SetTexture ("_BumpMap", normalTex);
					}

					renderer.sharedMaterial = baseMat;//new Material (options.baseMaterials.terrainImagegeryMaterial);
					renderer.sharedMaterial.mainTexture = tex;

					break;

				case GOTerrainImageryOptions.TerrainImageryMode.LowPoly_Texture:
					renderer.sharedMaterial = new Material(options.baseMaterials.lowPolyMaterial);
					renderer.sharedMaterial.mainTexture = tex;
					break;

				}
			}

		}	
		#endregion

		#region TERRAIN GENERATOR

		private static Mesh InitTerrain (Texture2D tex,Texture2D normalTex ,GOTerrainOptions options, GOTerrainTile parentTile, Vector2 offset, Vector2 resolution){

			Mesh terrainMesh = new Mesh ();
			terrainMesh.name = "Surface Mesh";

			Color32[] arcolors = tex.GetPixels32();
			Color32[] arcNormals = normalTex == null ? null : normalTex.GetPixels32();

			Vector3[] vertices;
			Vector3[] normals;
			Color[] colors;

			vertices = new Vector3[(((int)resolution.x +1) * ((int)resolution.y+1))];

			//Adiacent tiles
			GOTerrainTile top = parentTile.GetAdiacentTile(GOTerrainTile.GOTileBorder.Top);
			GOTerrainTile left = parentTile.GetAdiacentTile(GOTerrainTile.GOTileBorder.Left);
			GOTerrainTile right =parentTile.GetAdiacentTile(GOTerrainTile.GOTileBorder.Right);
			GOTerrainTile bottom =  parentTile.GetAdiacentTile(GOTerrainTile.GOTileBorder.Bottom);

			Mesh mfT = new Mesh();
			Mesh mfL = new Mesh();
			Mesh mfR = new Mesh();
			Mesh mfB = new Mesh();	

			/// Merging adiacent tile procedure
			bool tileT  = false; bool tileL = false; bool tileR = false; bool tileB = false;

			if (top  != null ) {
				MeshFilter mf = top.gameObject.GetComponent<MeshFilter> ();
				if (mf != null) {
					tileT = true;
					mfT = mf.sharedMesh;
				}
			}
			if (left!= null ) {

				MeshFilter mf = left.gameObject.GetComponent<MeshFilter> ();
				if (mf != null) {
					tileL = true;
					mfL = mf.sharedMesh;
				}
			}			
			if (right!= null) {
				MeshFilter mf = right.gameObject.GetComponent<MeshFilter> ();
				if (mf != null) {
					tileR = true;
					mfR = mf.sharedMesh;
				}
			}			
			if (bottom!= null) {
				MeshFilter mf = bottom.gameObject.GetComponent<MeshFilter> ();
				if (mf != null) {
					tileB = true;
					mfB = mf.sharedMesh;
				}
			}


			colors = new Color[vertices.Length];
			normals = new Vector3[vertices.Length];
			Vector2[] uv = new Vector2[vertices.Length];


			float stepSizeWidth = parentTile.tileWidth/ options.resolution;

			float stepSizeHeight = parentTile.tileHeight/ options.resolution;

			for (int v = 0, z = 0; z < (int)resolution.y + 1; z++) {
				for (int x = 0; x < (int)resolution.x +1; x++) {

					Color32 c32 = calculateColor (tex, arcolors, new Vector2 (x, z), offset, resolution);


					float height = ConvertColorToAltitude (c32, options);
					height = height * options.altitudeMultiplier;


					vertices[v] = new Vector3 (x * stepSizeWidth + offset.x  , height, 	z * stepSizeHeight + offset.y  );
					vertices [v] -= new Vector3 (parentTile.tileWidth/2, 0, parentTile.tileHeight / 2);

					colors [v] = c32;

					if (options.useNormals) {


						Color32 c32Normal = calculateColor (normalTex, arcNormals, new Vector2 (x, z), offset, resolution);

						normals [v] = new Vector3 (c32Normal.r, c32Normal.g, c32Normal.b);
					} else {
						normals [v] = Vector3.up;
					}


					uv[v] = new Vector2(x/resolution.x, z/resolution.y);

					v++;

				}
			}

			for (int v = 0, z = 0; z < (int)resolution.y + 1; z++) {
				for (int x = 0; x < (int)resolution.x + 1; x++) {

					if (tileT && z == (int)resolution.y) {
						int pos = x;

						vertices [v] = TrasformVectorReferences (parentTile,top,mfT.vertices[pos]); 
						} 

					if  (tileL && x == 0) {
						int pos = (z+1) * (int)resolution.x + z ;	
						vertices [v] = TrasformVectorReferences (parentTile,left,mfL.vertices[pos]); 

					}

					if  (tileR && x == (int)resolution.x) {
						int pos = z * ((int)resolution.x + 1) ;
						vertices [v] = TrasformVectorReferences (parentTile,right,mfR.vertices[pos]); 

					} 
					if(tileB && z == 0) {
						int pos = mfB.vertices.Count () - ((int)resolution.x + 1) + x;

						vertices [v] = TrasformVectorReferences (parentTile,bottom,mfB.vertices[pos]); 

					}
					
					v++;
				}
			}

			int[] triangles = new int[((int)resolution.x)*((int)resolution.y) *2 *3];
			float[] heightCheck = new float [6];
			for (int v = 0,t=0, z = 0; z < (int)resolution.y; z++,t++) {
				for (int x = 0; x < (int)resolution.x; x++,v+=6,t++) {

					triangles [v] = t;
					triangles [v + 1] = t + (int)resolution.x +1;
					triangles [v + 2] = t + 1;
					triangles [v + 3] = t + 1;
					triangles [v + 4] = t + (int)resolution.x +1;
					triangles [v + 5] = t + (int)resolution.x +2;

					//Checks spike map errors
					heightCheck[0] = vertices [triangles [v]].y;
					heightCheck[1] = vertices [triangles [v+1]].y;
					heightCheck[2] = vertices [triangles [v+2]].y;
					heightCheck[3] = vertices [triangles [v+3]].y;
					heightCheck[4] = vertices [triangles [v+4]].y;
					heightCheck[5] = vertices [triangles [v+5]].y;

					float mean = 0.0f;
					float meanAbs = 0.0f;
					float standardDeviation = 0.0f;
						
					foreach(float height in heightCheck){
						mean += height/ 6.0f;
						meanAbs += Mathf.Abs(height)/ 6.0f;
						standardDeviation += (height * height); 
					}
					standardDeviation = standardDeviation / 6.0f;

					float variance = standardDeviation;
					standardDeviation= Mathf.Sqrt (variance);
				
					float newMean = 0.0f;
					int meanCount = 0;
					for (int i = 0; i < heightCheck.Count (); i++) {
						
						if (variance > 100 && Mathf.Abs (heightCheck [i]) > standardDeviation + meanAbs) {
							//	CreateDebugSphere ( vertices[triangles[v+i]] + Vector3.up,"spikes STD" + standardDeviation + "  " + variance,10,null);
						} else {
							meanCount++;
							newMean += heightCheck [i];
						}
							
					}

					newMean = newMean / meanCount;

					for (int i = 0; i < heightCheck.Count (); i++) {

						if (variance > 200 && Mathf.Abs (heightCheck [i]) > standardDeviation + meanAbs) {
								vertices [triangles [v + i]] = new Vector3 (vertices [triangles [v + i]].x, newMean, vertices [triangles [v + i]].z);
						}
					}


				}
			}
				
				
			terrainMesh.vertices = vertices;
			terrainMesh.normals = normals;
			terrainMesh.uv = uv;
			terrainMesh.triangles = triangles;

			return terrainMesh;
		}


		private static Mesh ColorTerrain (Texture2D tex, GOTerrainOptions options, Vector2 offset, Vector2 resolution, Mesh terrainMesh){

			Color32[] arcolors = tex.GetPixels32();

			int[] triangles = terrainMesh.triangles;

			Color[] colors = new Color[terrainMesh.vertices.Length];

			for (int v = 0,t=0, z = 0; z < (int)resolution.y; z++,t++) {
				for (int x = 0; x < (int)resolution.x; x++,v+=6,t++) {
					
					Color32 c32 = calculateColor (tex, arcolors, new Vector2 (x, z), offset, resolution);

					float height = ConvertColorToAltitude (c32, options);

					if (options.generationMode == GOTerrainOptions.TerrainGenerationMode.GradientColor) {

						int compressStartMeter = options.gradientOptions.startAltitude;
						int compressFinishMeter = options.gradientOptions.endAltitude;
						float colorCalculated = 0.0f;

						if (compressStartMeter > compressFinishMeter) {Debug.LogWarning ("quantizedCompressionEnd must be greater then quantizedCompressionStart ");}

						if (height <= compressStartMeter) { 
							colorCalculated = 0.0f;
						} 
						else if (height > compressFinishMeter) {
							colorCalculated = 1.0f;
						} 
						else {
							colorCalculated = ((float)height - (float)compressStartMeter) / ((float)compressFinishMeter - (float)compressStartMeter);
						}
							
						float posterizedColor = ((int)(100 * colorCalculated)/((int)(100/(int)options.gradientOptions.posterizationLevels)))
							* (100/(int)options.gradientOptions.posterizationLevels)/100.0f;

						for (int i = 0; i < 6; i++) {	colors [triangles [v + i]] = options.gradientOptions.gradient.Evaluate (posterizedColor);}

					} 
					else if (options.generationMode == GOTerrainOptions.TerrainGenerationMode.TerrainImagery
						&& (/*options.imageryOptions.imageMode == GOTerrainImageryOptions.TerrainImageryMode.Color ||*/ options.imageryOptions.imageMode == GOTerrainImageryOptions.TerrainImageryMode.LowPoly_VertexColor) ) {
					
						for (int i = 0; i < 6; i++) {
							colors [triangles [v + i]] = c32;
						}
					} 
				}
			}

			terrainMesh.colors = colors;

			return terrainMesh;
		}

		private static Color32 calculateColor (Texture2D tex,Color32[] arcolors, Vector2 position, Vector2 offset, Vector2 resolution){

			Color32 color = Color.black;

			try {

			int xOff =  Mathf.FloorToInt(position.x * tex.width/resolution.x);
			int zOff =  Mathf.FloorToInt(position.y * tex.height/resolution.y);
			if (xOff > 255)	xOff = 255;
			if (zOff > 255)	zOff = 255;

			int colorFormula = xOff + (int)offset.x + (int)tex.width * (zOff + (int)offset.y);

			color = arcolors[colorFormula];

			} catch {
				int positionOverflowX = (int)position.x;
				int positionOverflowY = (int)position.y;

				if (positionOverflowX > resolution.x) {
					positionOverflowX = (int)resolution.x;
				}
				if (positionOverflowY > resolution.y) {
					positionOverflowY = (int)resolution.y;
				}
				int xOff =  Mathf.FloorToInt(positionOverflowX* tex.width/resolution.x);
				int zOff =  Mathf.FloorToInt(positionOverflowY* tex.height/resolution.y);
				if (xOff > 255)	xOff = 255;
				if (zOff > 255)	zOff = 255;

				int colorFormula = xOff + (int)offset.x + (int)tex.width * (zOff + (int)offset.y);

				color = arcolors[colorFormula];

			}
			return color;
		}
			
		#endregion

		#region Altitude API

		public static float ConvertColorToAltitude (Color32 c32, GOTerrainOptions options) {

			switch (options.elevationAPI) {

			case GOTerrainOptions.APIprovider.Mapzen :
				return (c32.r * 256.0f + c32.g + c32.b / 256.0f) - 32768.0f;

			case GOTerrainOptions.APIprovider.Mapbox :
				return -10000.0f + ((c32.r * 256.0f * 256 + c32.g * 256 + c32.b)*0.1f);
			}

			return 0;
		}

		#endregion

		#region CONVERSIONS

		public static float FindAltitudeForVector (Vector3 inputVector, Texture2D tex, GOTerrainTile parentTile, Vector3 tileOriginVector, GOTerrainOptions options) {

			if (tex == null)
				return 0;

			float stepSizeWidth = parentTile.tileWidth;
			float stepSizeHeight = parentTile.tileHeight;

			//Move the tile origin to match the DEM origin
			Vector2 fixedOrigin = tileOriginVector.ToVector2xz() - new Vector2 (0,parentTile.tileHeight);

			float x = 0;
			float z = 0;

			//Convert coords to unity 
			Vector2 input = inputVector;

			//Compute the distance for each axis
			x = input.x - fixedOrigin.x;
			z = input.y - fixedOrigin.y;

			//Adapt the values to the stepsize
			x = tex.width * x / stepSizeWidth;
			z = tex.height * z / stepSizeHeight;

			Color32[] arcolors = tex.GetPixels32();
			Color32 c32 = calculateColor (tex, arcolors, new Vector2 (x, z), Vector2.zero, new Vector2(tex.width,tex.height));

			float height = ConvertColorToAltitude(c32,options);
			height = height * options.altitudeMultiplier;

			return height;

		}

		public static float FindAltitudeForCoordinate (Coordinates inputCoordinate, Texture2D tex, GOTerrainTile parentTile, Coordinates tileOrigin, GOTerrainOptions options) {

			if (tex == null)
				return 0;
			
			float stepSizeWidth = parentTile.tileWidth;
			float stepSizeHeight = parentTile.tileHeight;

			//Move the tile origin to match the DEM origin
			Vector2 fixedOrigin = tileOrigin.convertCoordinateToVector2D () - new Vector2 (0,parentTile.tileHeight);

			float x = 0;
			float z = 0;

			//Convert coords to unity 
			Vector2 input = inputCoordinate.convertCoordinateToVector2D ();

			//Compute the distance for each axis
			x = input.x - fixedOrigin.x;
			z = input.y - fixedOrigin.y;

			//Adapt the values to the stepsize
			x = tex.width * x / stepSizeWidth;
			z = tex.height * z / stepSizeHeight;

			Color32[] arcolors = tex.GetPixels32();
			Color32 c32 = calculateColor (tex, arcolors, new Vector2 (x, z), Vector2.zero, new Vector2(tex.width,tex.height));

			float height = ConvertColorToAltitude (c32, options);
			height = height * options.altitudeMultiplier;

			return height;

		}

		#endregion

		public static IEnumerable<int> To ( int from, int to)
		{
			if (from < to)
			{
				while (from <= to)
				{
					yield return from++;
				}
			}
			else
			{
				while (from >= to)
				{
					yield return from--;
				}
			}
		}


		public static List<int> tableHeightGeneration(){
			List<int> table = new List<int>();
			foreach (int i in To(0,10)) {	table.Add (-11000 + i * 1000);	}

				table.Add (-100);
				table.Add (-50);
				table.Add (-20);
				table.Add (-10);
				table.Add (-1);

			foreach (int a in To(0,149)) {table.Add (20 * a);}
			foreach (int b in To(0,59)) {table.Add (3000 +50 * b);}
			foreach (int c in To(0,29)) {table.Add (6000 +100 * c);	}
		
			return table;
			}

		static void CreateDebugSphere ( Vector3 position , string name, float scale, GameObject parent){

			GameObject test2 = GameObject.CreatePrimitive (PrimitiveType.Sphere);
			test2.name = name;
			test2.transform.position = position; // ector3.Scale (position, new Vector3 (1, 0, 1));
			test2.transform.localScale = new Vector3 (scale,scale,scale);

		}
		public static void CreateDebugPosition(Vector3 position, Color color, int height){
		
			Debug.DrawLine (position,position+Vector3.up*1000,color,height);
		}

		static Vector3 TrasformVectorReferences (GOTerrainTile parent, GOTerrainTile local, Vector3 position){
		
			return parent.gameObject.transform.InverseTransformPoint ( local.gameObject.transform.TransformPoint (position));
		}

		public static bool RandomBool()	{
			if ( Random.value >= 0.5)
			{
				return true;
			}
			return false;
		}
	}


		
}



