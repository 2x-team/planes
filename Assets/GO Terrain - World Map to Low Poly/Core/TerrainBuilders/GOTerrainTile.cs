using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;
using MiniJSON;

using UnityEngine.Profiling;

using GoShared;

namespace GoTerrain
{
	[ExecuteInEditMode]
	public class GOTerrainTile : MonoBehaviour
	{
		public Coordinates tileCenter;
		public Vector2 tileCoordinates;
		public Vector3 tileOriginFixed;
		public float tileWidth;
		public float tileHeight;
		public float diagonalLenght;

		public Texture2D terrainImage = null;
		public Texture2D normalsImage = null;
		public Texture2D satelliteImage = null;

		object vectorData = null;
		bool vectorDataDownloaded;

		public bool TileLoaded = false;
		bool TileRequestError = false;

		[HideInInspector]
		public List<Vector3> vertices;

		public GOTerrain map;

		public IEnumerator LoadTerrainData(object m, Coordinates tilecenter, int zoom, bool delayedLoad)
		{

			map = (GOTerrain)m;

			tileHeight = Vector3.Distance (vertices [0], vertices [1]);
			tileWidth = Vector3.Distance (vertices [1], vertices [2]);

			#if !UNITY_WEBPLAYER
		
			List <IEnumerator> operations = new List<IEnumerator>();

			operations.Add (DownloadTerrainData());

			if (map.goTerrainOptions.useNormals)
				operations.Add (DownloadNormalsData());

			//Download vector data
			if (map.goTerrainOptions.generationMode == GOTerrainOptions.TerrainGenerationMode.Vector || map.goTerrainOptions.loadEnvironment) {
				operations.Add (DownloadVectorData());
			} else if (map.goTerrainOptions.generationMode == GOTerrainOptions.TerrainGenerationMode.TerrainImagery)	{
				operations.Add (DownloadSatelliteData());
			} 


			if (Application.isPlaying) {
				operations.Add (BuildTerrain());
				IEnumerator chain = GOCoroutineUtils.Chain(this,operations.ToArray());
				yield return StartCoroutine (chain);

			} else {
				yield return GORoutine.start((GOCoroutineUtils.Chain(
					this,
					operations.ToArray()
				)
				),this);
			}


			if (map.goTerrainOptions.addDebugMarkers){
				ShowDebugMarkers();
			}
			#endif
		}

		#if UNITY_EDITOR
		void Update () {

			if (Application.isPlaying || TileLoaded || !map)
				return;

			bool condition = terrainImage == null;

			if (map.goTerrainOptions.useNormals)
				condition = condition || normalsImage == null;

			if (map.goTerrainOptions.generationMode == GOTerrainOptions.TerrainGenerationMode.Vector || map.goTerrainOptions.loadEnvironment) {
				condition = condition || !vectorDataDownloaded;
			} else if (map.goTerrainOptions.generationMode == GOTerrainOptions.TerrainGenerationMode.TerrainImagery)	{
				condition = condition || satelliteImage == null;
			} 

			if(!condition)
			{
				TileLoaded = true;
				GORoutine.start (BuildTerrain (),this);
			}
		}
		#endif

		public IEnumerator BuildTerrain()
		{

			Debug.LogWarning("COROUTINE BUILD TERRAIN: "+ tileCoordinates);


			if (TileRequestError)
				yield break;

			Profiler.BeginSample ("Build Terrain");
			tileOriginFixed = tileCenter.tileOrigin (map.zoomLevel).convertCoordinateToVector () - new Vector3 (0,0,tileWidth);
			GOTerrainBuilder.BuildTerrain (this, terrainImage,normalsImage, map.goTerrainOptions);
			Profiler.EndSample();


			Profiler.BeginSample ("Color Terrain");
			//Load terrain color
			if (map.goTerrainOptions.generationMode == GOTerrainOptions.TerrainGenerationMode.TerrainImagery ) {
				GOTerrainBuilder.ColorTerrainTile (gameObject, satelliteImage,normalsImage, map.goTerrainOptions, tileWidth);
			} 
			else {
				GOTerrainBuilder.ColorTerrainTile (gameObject, terrainImage,normalsImage, map.goTerrainOptions, tileWidth);
			}
			Profiler.EndSample();


			//Trigger event
			if (map.didLoadTile != null) {
				map.didLoadTile.Invoke (this);
			}

			//Load environment
			if (map.goTerrainOptions.generationMode == GOTerrainOptions.TerrainGenerationMode.Vector || map.goTerrainOptions.loadEnvironment) {

				if (map.goTerrainOptions.generationMode == GOTerrainOptions.TerrainGenerationMode.Vector) {
					foreach (MeshRenderer mr in GetComponentsInChildren<MeshRenderer>()) {
						mr.material = map.goTerrainOptions.vectorOptions.earthMaterial;
					}
				}

				yield return StartCoroutine (BuildVectorFeatures ());

				#if GOLINK
				//[GOLINK] Trigger GOMap Tile creation (This requires GOMap! https://www.assetstore.unity3d.com/en/#!/content/68889) 
				if (vectorData != null && map.goMap != null) {
					yield return StartCoroutine(map.goMap.createTileWithPreloadedData(tileCenter.tileCoordinates(map.zoomLevel),map.zoomLevel,true,vectorData));
				}
				#endif
			} 


			yield return null;
		}

		public IEnumerator BuildVectorFeatures()
		{


			string[] layers = new string[] { "water", "landuse" };//, "earth"}

			foreach (string layer in layers) {

				IDictionary layerData = null;

				if (vectorData != null && ((IDictionary)vectorData).Contains (layer)) {
					
					layerData = (IDictionary)((IDictionary)vectorData) [layer];
					GOTerrainVectorLoader vectorLoader = new GOTerrainVectorLoader (layerData, layer, this);

					Debug.LogWarning("COROUTINE VECTOR LOAD: "+ tileCoordinates);

					if (Application.isPlaying)
						yield return StartCoroutine (vectorLoader.BuildFeatures ());
					else
						yield return GORoutine.start (vectorLoader.BuildFeatures (),this);
				} 
				else {
					Debug.LogError (vectorData);
				}
			}

			yield return StartCoroutine(GOTerrainVectorLoader.SpawnClouds(this));

			yield return null;
		}

		#region Utils

		public enum GOTileBorder  {
			Top,
			Bottom,
			Left,
			Right
		}
		public GOTerrainTile GetAdiacentTile(GOTileBorder border) {
		
			Vector2 tileModifier = Vector2.zero;

			switch (border)
			{
			case GOTileBorder.Top:
				tileModifier = new Vector2 (0, -1);
				break;
			case GOTileBorder.Left:
				tileModifier = new Vector2 (-1, 0);
				break;
			case GOTileBorder.Bottom:
				tileModifier = new Vector2 (0, 1);
				break;
			case GOTileBorder.Right:
				tileModifier = new Vector2 (1, 0);
				break;
			default:
				break;
			}

			foreach (GOTerrainTile tile in map.GetComponentsInChildren<GOTerrainTile>()) {
				if (tile.tileCoordinates == this.tileCoordinates + tileModifier) {

					//Debug.Log ("Tile to the " + border.ToString () + " is: " + tile.tileCoordinates);
					return tile;
				}
			}
			return null;
		}

		#endregion

		#region REQUESTS

		public IEnumerator DownloadTerrainData()
		{

			var baseUrl = "";
			var extension = "";
			var completeUrl = "";
			var filename = "";

			//Download terrain data
			Vector2 realPos = tileCenter.tileCoordinates (map.zoomLevel);
			var tileurl = map.zoomLevel + "/" + realPos.x + "/" + realPos.y;

			if (map.goTerrainOptions.elevationAPI == GOTerrainOptions.APIprovider.Mapzen) {
				baseUrl = "https://tile.mapzen.com/mapzen/terrain/v1/terrarium/";
				extension = ".png";
				completeUrl = baseUrl + tileurl + extension;
				filename = "[Mapzen]" + gameObject.name;
				if (map.mapzen_api_key != null && map.mapzen_api_key != "") {
					string u = completeUrl + "?api_key=" + map.mapzen_api_key;
					completeUrl = u;
				}

			} else if (map.goTerrainOptions.elevationAPI == GOTerrainOptions.APIprovider.Mapbox) {
				baseUrl = "https://api.mapbox.com/v4/mapbox.terrain-rgb/";
				extension = ".pngraw";
				completeUrl = baseUrl + tileurl + extension; 
				filename = "[Mapbox]" + gameObject.name;
				if (map.mapbox_accessToken != null && map.mapbox_accessToken != "") {
					string u = completeUrl + "?access_token=" + map.mapbox_accessToken;
					completeUrl = u;
				}
			}

			return GOUrlRequest.getRequest (this, completeUrl, map.useCache, filename, (byte[] bytes, string text, string error) => {

				if (error == null) {
					terrainImage = new Texture2D (256, 256);
					terrainImage.wrapMode = TextureWrapMode.Clamp;
					terrainImage.LoadImage (bytes);
				} else {
					TileRequestError = true;
				}

//				Debug.Log ("[GOTerrainTILE] Download terrain data " + realPos);

			});
		}

		public IEnumerator DownloadNormalsData()
		{

			//Normals data
			Vector2 realPos = tileCenter.tileCoordinates (map.zoomLevel);
			var tileurl = map.zoomLevel + "/" + realPos.x + "/" + realPos.y;
			var baseUrlNormals = "https://tile.mapzen.com/mapzen/terrain/v1/normal/";
			var extension = ".png";
			var normalsUrl = baseUrlNormals + tileurl + extension; 
			string filenameNormals = "[Normal]"+gameObject.name;

			if (map.mapzen_api_key != null && map.mapzen_api_key != "") {
				string u = normalsUrl + "?api_key=" + map.mapzen_api_key;
				normalsUrl = u;
			}

			return GOUrlRequest.getRequest (this, normalsUrl, map.useCache, filenameNormals, (byte[] bytes, string text, string error) => {

				if (error == null) {
					normalsImage = new Texture2D (256, 256);
					normalsImage.wrapMode = TextureWrapMode.Clamp;
					normalsImage.LoadImage (bytes);

//					Debug.Log ("[GOTerrainTILE] Download normals data " + realPos);

				}else {
					TileRequestError = true;
				}
			});
		}

		public IEnumerator DownloadSatelliteData()
		{

			Vector2 realPos = tileCenter.tileCoordinates (map.zoomLevel);

			string completeurl;
			string filename;
			string tileurl;
			string baseUrl;
			string sizeUrl;
			string extension;

			switch (map.goTerrainOptions.imageryOptions.imageService)
			{
			case GOTerrainImageryOptions.TerrainImageryType.Satellite:
				tileurl = tileCenter.longitude + "," + tileCenter.latitude + "," +map.zoomLevel;
				baseUrl = "https://api.mapbox.com/v4/mapbox.satellite/";
				sizeUrl = "/256x256.jpg?access_token="+map.mapbox_accessToken;
				completeurl = baseUrl + tileurl + sizeUrl; 
				filename = "[Satellite]"+gameObject.name;
//				if (map.mapbox_accessToken != null && map.mapbox_accessToken != "") {
//					string u = completeurl + "?access_token=" + map.mapbox_accessToken;
//					completeurl = u;
//				}
				break;
			case GOTerrainImageryOptions.TerrainImageryType.Watercolor:
				tileurl = map.zoomLevel + "/" + realPos.x + "/" + realPos.y;
				baseUrl = "https://stamen-tiles.a.ssl.fastly.net/watercolor/";
				extension = ".png";
				completeurl = baseUrl + tileurl + extension; 
				filename = "[Watercolor]"+gameObject.name;
				break;
			case GOTerrainImageryOptions.TerrainImageryType.Terrain:
				tileurl = map.zoomLevel + "/" + realPos.x + "/" + realPos.y;
				baseUrl = "http://tile.stamen.com/terrain-background/";
				extension = ".png";
				completeurl = baseUrl + tileurl + extension; 
				filename = "[Terrain]"+gameObject.name;
				break;
//			case GOTerrainImageryOptions.TerrainImageryType.Toner:
//				tileurl = map.zoomLevel + "/" + realPos.x + "/" + realPos.y;
//				baseUrl = "http://a.tile.stamen.com/toner/";
//				extension = ".png";
//				completeurl = baseUrl + tileurl + extension; 
//				filename = "[Toner]"+gameObject.name;
//				break;
//			case GOTerrainImageryOptions.TerrainImageryType.Physical:
//				tileurl = zoom + "/" + realPos.x + "/" + realPos.y;
//				baseUrl = "https://services.arcgisonline.com/ArcGIS/rest/services/World_Physical_Map/MapServer/tile/";
//				extension = ".png";
//				completeurl = baseUrl + tileurl + extension; 
//				filename = "[Physical]"+gameObject.name;
//				break;
			default:
				completeurl = null;
				filename = null;
				break;
			}
		//	Debug.Log ("Complete url "+completeurl);

			return GOUrlRequest.getRequest (this, completeurl, map.useCache, filename, (byte[] bytes, string text, string error) => {

				if (error == null) {
					satelliteImage = new Texture2D (256, 256);
					satelliteImage.wrapMode = TextureWrapMode.Clamp;
					satelliteImage.LoadImage (bytes);

//					Debug.Log ("[GOTerrainTILE] Download satellite data " + realPos);

				}else {
					TileRequestError = true;
				}
			});
		}


		public IEnumerator DownloadVectorData()
		{

			Vector2 realPos = tileCenter.tileCoordinates (map.zoomLevel);
			string filename = "[Vector]"+gameObject.name;
			var tileurl = realPos.x + "/" + realPos.y;
			var baseUrl = "https://tile.mapzen.com/mapzen/vector/v1/";
			var url = baseUrl + "all/"+map.zoomLevel+"/";
			var completeurl = url + tileurl + ".json"; 

			if (map.mapzen_api_key != null && map.mapzen_api_key != "") {
				string u = completeurl + "?api_key=" + map.mapzen_api_key;
				completeurl = u;
			}

			return GOUrlRequest.jsonRequest (this, completeurl, map.useCache, filename, (Dictionary<string,object> response, string error) => {

				if (error == null) {
					vectorData = response;
					vectorDataDownloaded = true;
			#if UNITY_EDITOR
					Update();
			#endif
//					Debug.Log ("[GOTerrainTILE] Download vector data " + completeurl);

				}else {
					TileRequestError = true;
				}
			});

		}


		#endregion

		#region DEBUG

		private void ShowDebugMarkers () {

//			//ORIGIN CHECK coordinates
//			Coordinates vertCoords =  Coordinates.convertVectorToCoordinates(tileOriginFixed);
//			float height = GOTerrainBuilder.FindAltitudeForCoordinate (vertCoords, terrainImage, tileWidth, tileCenter.tileOrigin(map.zoomLevel));
//			Debug.LogWarning ("Height for tile vertex is: " + height);
//
//			//ORIGIN CHECK vector
//			height = GOTerrainBuilder.FindAltitudeForVector (tileOriginFixed, terrainImage, tileWidth, tileCenter.tileOrigin (map.zoomLevel).convertCoordinateToVector ());
//			Debug.LogWarning ("Height for tile vertex is: " + height);

			//VERTICES CHECK (coordinates)
//			foreach (Vector3 vertex in vertices) {

//				Vector3 vertexInWorld = transform.TransformPoint(vertex);
//				Debug.LogWarning ("vertex is: " + vertexInWorld.x+","+vertexInWorld.z);
				//					Coordinates vertCoords =  Coordinates.convertVectorToCoordinates(vertexInWorld);
				//					float height = TerrainBuilder.FindAltitudeForCoordinate (vertCoords, terrainImage, tileWidth, tileCenter.tileOrigin(map.zoomLevel));
//				float height = GOTerrainBuilder.FindAltitudeForVector (vertexInWorld, terrainImage, tileWidth, tileCenter.tileOrigin(map.zoomLevel).convertCoordinateToVector(),map.goTerrainOptions);
//				float height = 0;
//				Debug.LogWarning ("Height for tile vertex is: " + height);
//
//				//Spheres
//				GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
//				sphere.transform.localScale = Vector3.one * 20;
//				sphere.transform.position = new Vector3 (vertexInWorld.x, height, vertexInWorld.z);
//
//			}
		}

		public IEnumerator BuildDebugPlane() {

			MeshFilter filter = gameObject.AddComponent<MeshFilter>();
			MeshRenderer renderer = gameObject.AddComponent<MeshRenderer>();

			Mesh mesh = new Mesh();

			Vector2[] uv = new Vector2[]
			{
				new Vector2(0, 0),
				new Vector2(0, 1),
				new Vector2(1, 1),
				new Vector2(1, 0),
			};

			int[] triangles = new int[]
			{
				0, 1, 3,
				3, 1, 2,
			};

			mesh.vertices = vertices.ToArray();
			mesh.uv = uv;
			mesh.triangles = triangles;

			mesh.RecalculateNormals ();

			filter.mesh = mesh;
			renderer.material = null;

			GOTerrainBuilder.ColorTerrainTile (gameObject, satelliteImage,normalsImage, map.goTerrainOptions, tileWidth);

			yield return null;
		}

		#endregion
	}
}
