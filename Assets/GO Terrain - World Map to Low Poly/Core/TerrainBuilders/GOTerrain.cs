﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using GoShared;

#if GOLINK
//[GOLINK] GOMap link (This requires GOMap! https://www.assetstore.unity3d.com/en/#!/content/68889) 
using GoMap;
#endif

namespace GoTerrain {

	[ExecuteInEditMode]
	[System.Serializable]
	public class GOTerrain : MonoBehaviour {

		#if GOLINK
		//[GOLINK] GOMap link (This requires GOMap! https://www.assetstore.unity3d.com/en/#!/content/68889) 
		[HideInInspector] public GOMap goMap;
		//
		#endif

		public LocationManager locationManager;
		[Range(0, 10)] public int tileBuffer = 2;
		[ShowOnly]public int zoomLevel = 0;
		public bool useCache = true;
		[Regex (@"^(?!\s*$).+", "Please insert your Mapzen API key")]
		public string mapzen_api_key = "";
		[Regex (@"^(?!\s*$).+", "Please insert your MapBox Access Token")]
		public string mapbox_accessToken = "";
		Vector2 Center_tileCoords;
		List <GOTerrainTile> tiles = new List<GOTerrainTile>();
		public GOTerrainOptions goTerrainOptions;
		public GOTerrainTileEvent didLoadTile;

		void Awake () 
		{

			locationManager.onOriginSet += OnOriginSet;
			locationManager.onLocationChanged += OnLocationChanged;

			#if GOLINK
			//[GOLINK] GOTerrain link (This requires GOTerrain! -Coming Soon- ) 
			goMap = FindObjectOfType<GOMap>();
			#endif

			zoomLevel = locationManager.zoomLevel;	

			if (mapzen_api_key == null || mapzen_api_key == "") {
				Debug.LogWarning ("[GOTerrain] Mapzen api key is missing, GET IT HERE: https://mapzen.com/developers");
			}
			if (mapbox_accessToken == null || mapbox_accessToken == "") {
				Debug.LogWarning ("[GOTerrain] MapBox access token required for Satellite images, GET IT HERE: https://www.mapbox.com/help/create-api-access-token/");
			}

			#if UNITY_WEBPLAYER
			Debug.LogError ("GOTerrain is NOT supported in the webplayer! Please switch platform in the build settings window.");
			#endif		
		}

		public IEnumerator ReloadMap (Coordinates location, bool delayed) {

			//Get SmartTiles
			List <Vector2> tileList = location.adiacentNTiles(zoomLevel,tileBuffer);

			List <GOTerrainTile> newTiles = new List<GOTerrainTile> ();

			// Create new tiles
			foreach (Vector2 tileCoords in tileList) {

				if (!isSmartTileAlreadyCreated (tileCoords, zoomLevel)) {

					GOTerrainTile adiacentSmartTile = createTerrainTileObject (tileCoords, zoomLevel);
					adiacentSmartTile.tileCenter = new Coordinates (tileCoords, zoomLevel);
					adiacentSmartTile.diagonalLenght = adiacentSmartTile.tileCenter.diagonalLenght(zoomLevel);
					adiacentSmartTile.tileCoordinates = adiacentSmartTile.tileCenter.tileCoordinates(zoomLevel);
					adiacentSmartTile.gameObject.transform.position = adiacentSmartTile.tileCenter.convertCoordinateToVector();
					List<Vector3> fixedVerts = new List<Vector3> ();
					foreach (Vector3 vert in adiacentSmartTile.tileCenter.tileVertices (zoomLevel)) {
						fixedVerts.Add(adiacentSmartTile.transform.InverseTransformPoint(vert));
					}
					adiacentSmartTile.vertices = fixedVerts;

					newTiles.Add (adiacentSmartTile);

				}
			}


			foreach (GOTerrainTile tile in newTiles) {

				#if !UNITY_WEBPLAYER 

//				if (tile != null && FileHandler.Exist (tile.gameObject.name) && useCache) {
//					yield return tile.StartCoroutine(tile.LoadTerrainData(this, tile.tileCenter, zoomLevel,delayed));
//				} else {
					yield return tile.StartCoroutine(tile.LoadTerrainData(this, tile.tileCenter, zoomLevel,delayed));
//				}

				#endif
			}

			//Destroy far tiles
			List <Vector2> tileListForDestroy = location.adiacentNTiles(zoomLevel,tileBuffer);
			yield return StartCoroutine (DestroyTiles(tileListForDestroy));

			#if GOLINK
			//[GOLINK] GOTerrain link (This requires GOTerrain! -Coming Soon- ) 
			if (goMap != null) {
				yield return goMap.StartCoroutine (goMap.DestroyTiles (tileListForDestroy));
			}
			#endif

			yield return null;
		}

		IEnumerator DestroyTiles (List <Vector2> list) {

			try {
				List <string> tileListNames = new List <string> ();
				foreach (Vector2 v in list) {
					string name = "[GOTerrain] "+v.x + "-" + v.y + "-" + zoomLevel;
					tileListNames.Add (name);
				}

				List <GOTerrainTile> toDestroy = new List<GOTerrainTile> ();
				foreach (GOTerrainTile tile in tiles) {

					if (!tileListNames.Contains (tile.name)) {
						toDestroy.Add (tile);
					}
				}
				for (int i = 0; i < toDestroy.Count; i++) {
					GOTerrainTile tile = toDestroy [i];
					tiles.Remove (tile);
					GameObject.Destroy (tile.gameObject,i);
				}
			} catch  {

			}
			yield return null;
		}

		void OnLocationChanged (Coordinates currentLocation) {
			StartCoroutine(ReloadMap (currentLocation,true));
		}

		void OnOriginSet (Coordinates currentLocation) {
			StartCoroutine(ReloadMap (currentLocation,false));
		}

		bool isSmartTileAlreadyCreated (Vector2 tileCoords, int Zoom) {

			string name = "[GOTerrain] "+tileCoords.x+ "-" + tileCoords.y+ "-" + zoomLevel;
			return transform.Find (name);
		}

		GOTerrainTile createTerrainTileObject (Vector2 tileCoords, int Zoom) {
			
			GameObject tileObj = new GameObject("[GOTerrain] "+tileCoords.x+ "-" + tileCoords.y+ "-" + zoomLevel);
			tileObj.transform.parent = gameObject.transform;
			GOTerrainTile tile = tileObj.AddComponent<GOTerrainTile> ();
			tiles.Add(tile);

			return tile;
		}

		#region Height finders & raycast


		public float FindAltitudeForCoordinates(Coordinates coordinates) {

			foreach (GOTerrainTile t in tiles) {
				if (t.tileCenter.isEqualToCoordinate(coordinates.tileCenter(zoomLevel))) {
					return GOTerrainBuilder.FindAltitudeForCoordinate (coordinates, t.terrainImage, t, t.tileCenter.tileOrigin (zoomLevel),goTerrainOptions);
				} 
			}
			return 0f;
		}

		public float FindAltitudeForVector(Vector3 vector) {

			Coordinates coordinates = Coordinates.convertVectorToCoordinates(vector);

			foreach (GOTerrainTile t in tiles) {
				if (t.tileCenter.isEqualToCoordinate(coordinates.tileCenter(zoomLevel))) {
					return GOTerrainBuilder.FindAltitudeForCoordinate (coordinates, t.terrainImage, t, t.tileCenter.tileOrigin (zoomLevel),goTerrainOptions);
				} 
			}
			return 0f;
		}

		public float FindAltitudeForVectorInTile(Vector3 vector, GOTerrainTile t) {

			Coordinates coordinates = Coordinates.convertVectorToCoordinates(vector);
			return GOTerrainBuilder.FindAltitudeForCoordinate (coordinates, t.terrainImage, t, t.tileCenter.tileOrigin (zoomLevel),goTerrainOptions);

		}

		public static float RaycastAltitudeForVector(Vector3 vector) {

			RaycastHit hit;
			Ray ray = new Ray (vector+ new Vector3(0,10000,0), Vector3.down);
			if (Physics.Raycast (ray, out hit)) {
				return hit.point.y;
			}
			return vector.y;
		}

		public static bool IsPositionAboveWater(Vector3 vector) {

			LayerMask watermask = LayerMask.GetMask ("Water");
			RaycastHit hit;
			if ( Physics.Raycast(vector+ new Vector3(0,10000,0), Vector3.down, out hit, Mathf.Infinity, watermask) )
			{
				return true;
			} 

			return false;
				
		}

		#endregion

		#region Drop Pin

		public void dropPin(double lat, double lng, GameObject go) {

			Transform pins = transform.FindChild ("Pins");
			if (pins == null) {
				pins = new GameObject ("Pins").transform;
				pins.parent = transform;
			}

			Coordinates coordinates = new Coordinates (lat, lng,0);
			Vector3 pos = coordinates.convertCoordinateToVector();
			float altitude = GOTerrain.RaycastAltitudeForVector (pos);
			pos.y = altitude;
			go.transform.localPosition = pos;
			go.transform.parent = pins;	
		}

		public void dropPinAtAltitude(double lat, double lng, double altitude, GameObject go) {

			Transform pins = transform.FindChild ("Pins");
			if (pins == null) {
				pins = new GameObject ("Pins").transform;
				pins.parent = transform;
			}

			Coordinates coordinates = new Coordinates (lat, lng, altitude);
			Vector3 pos = coordinates.convertCoordinateToVector();
			go.transform.localPosition = pos;
			go.transform.parent = pins;	
		}

		#endregion
			
		#region Editor Map Builder

		public void BuildInsideEditor () {

			//This fixes the map origin
			locationManager.LoadDemoLocation ();

			//Start load routine (This might take some time...)
			IEnumerator routine = ReloadMap (locationManager.demo_CenterWorldCoordinates.tileCenter (locationManager.zoomLevel), false);
			GoShared.GORoutine.start (routine,this);

		}

		public void TestEditorWWW () {
			
			#if UNITY_EDITOR
			StartCoroutine (GOUrlRequest.testRequest("https://tile.mapzen.com/mapzen/vector/v1//buildings,landuse,water,roads/17/70076/48701.json", delegate {
				Debug.Log ("Request working");
			}));
			#endif
		}
		#endregion
	}
}