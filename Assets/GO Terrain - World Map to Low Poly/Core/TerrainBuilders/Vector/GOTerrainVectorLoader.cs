﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.IO;

using GoShared;

namespace GoTerrain {

	public class GOTerrainVectorLoader
	{

		public IDictionary layerData;
		public string layerName;
		public GOTerrainTile tile;

		public GOTerrainVectorLoader(IDictionary _layerData, string _layerName, GOTerrainTile _tile) // To be moved in GoTerrainLayer
		{
			layerData = _layerData;
			layerName = _layerName;
			tile = _tile;
		}


		public IEnumerator BuildFeatures() {

			GameObject parent = new GameObject ();
			parent.name = layerName;
			parent.transform.parent = tile.transform;
			//			parent.SetActive (!layer.startInactive);

			IList features = (IList)layerData ["features"];
			if (features == null)
				yield break;

			foreach (IDictionary geo in features) {

				IDictionary geometry = (IDictionary)geo ["geometry"];
				IDictionary properties = (IDictionary)geo ["properties"];
				string type = (string)geometry ["type"];
				string kind = (string)properties ["kind"];

//				var id = properties ["id"]; 

				if (type == "MultiLineString") {
//					IList lines = new List<object>();
//					lines = (IList)geometry ["coordinates"];
//					foreach (IList coordinates in lines) {
//						//						StartCoroutine (CreateLine (parent, kind, type, coordinates, properties, layer, delayedLoad));
//					}
				} 

				else if (type == "LineString") {
//					IList coordinates = (IList)geometry ["coordinates"];
					//						StartCoroutine (CreateLine (parent, kind, type, coordinates, properties, layer, delayedLoad));
				} 

				else if (type == "Polygon") {

					List <object> shapes = new List<object>();
					shapes = (List <object>)geometry["coordinates"];

					IList subject = null;
					List<object> clips = null;
					if (shapes.Count == 1) {
						subject = (List<object>)shapes[0];
					} else if (shapes.Count > 1) {
						subject = (List<object>)shapes[0];
						clips = shapes.GetRange (1, shapes.Count - 1);
					} else {
						continue;
					}

					yield return tile.StartCoroutine (CreateFeature (parent, kind, type, subject, clips, properties));			
				}

				if (type == "MultiPolygon") {

					GameObject multi = new GameObject ("MultiPolygon");
					multi.transform.parent = parent.transform;

					IList shapes = new List<object>();
					shapes = (IList)geometry["coordinates"];

					foreach (List<object> polygon in shapes) {

						IList subject = null;
						List<object> clips = null;
						if (polygon.Count > 0) {
							subject = (List<object>)polygon[0];
						} else if (polygon.Count > 1) {
							clips = polygon.GetRange (1, polygon.Count - 1);
						} else {
							continue;
						}

						yield return tile.StartCoroutine (CreateFeature (multi, kind, type, subject, clips,properties));	
					}
				}
			}
				
			yield return null;
		}


		private IEnumerator CreateFeature(GameObject parent, string kind, string type, IList subject, List<object> clips, IDictionary properties)
		{

//			try
//			{

			GameObject feature = null;
			float distance = 0f;
			Int64 sort;
			if (properties.Contains("sort_key")) {
				sort = (Int64)properties["sort_key"];
			} else sort = (Int64)properties["sort_rank"];


			if (distance == 0f) {
				distance = sort / 100.0f;
			}

			if (layerName == "water") {		

				feature = GOWaterTerrain.BuildWaterTerrain (parent ,kind, subject, clips, tile);
				if (feature == null) {
					yield break;
				}
			} 
			else if (layerName == "landuse") {
				
				feature = GOLanduseTerrain.BuildLanduseTerrain (parent ,kind, subject, clips, tile, distance, properties);
				yield return null;
				if (feature == null) {
					yield break;
				}			
			} 
				
			feature.transform.position += new Vector3 (0, distance, 0);

			GOAttributes attributes = feature.AddComponent<GOAttributes>();
			attributes.type = type;
			attributes.loadWithDictionary((Dictionary<string,object>)properties);

			AddObjectToLayerMask (UppercaseFirst(layerName), feature);

//			}
//			catch (Exception ex)
//			{
//				Debug.LogWarning(kind + "  " +ex);
//			}

			yield return null;
		}

		private void AddObjectToLayerMask (string layer, GameObject obj) {

			LayerMask mask = LayerMask.NameToLayer (layer);
			if (mask.value > 0 && mask.value < 31) {
				obj.layer = LayerMask.NameToLayer (layer);
			} else {
				Debug.LogWarning ("[GOTerrain] Please create layer masks before running GoTerrain. Please create a new layer in Unity named: \""+layer+"\".");
			}
		}

		string UppercaseFirst(string s){
			if (string.IsNullOrEmpty(s))
			{
				return string.Empty;
			}
			char[] a = s.ToCharArray();
			a[0] = char.ToUpper(a[0]);
			return new string(a);
		}


		public static IEnumerator SpawnClouds (GOTerrainTile tile) {

			GOTerrainOptions options = tile.map.goTerrainOptions;
			if (options.vectorOptions.cloudPrefabs.Length == 0) {
				yield return null;
			} else {

				int spawn = UnityEngine.Random.Range (0, 2);
				if (spawn == 0) {
					float y = UnityEngine.Random.Range (250, 350);
					Vector3 pos = tile.tileCenter.convertCoordinateToVector ();
					pos.y = y+GOTerrain.RaycastAltitudeForVector(pos);
					int n = UnityEngine.Random.Range (0, options.vectorOptions.cloudPrefabs.Length);

					GameObject prefab = options.vectorOptions.cloudPrefabs [n];
					var randomRotation =  new Vector3( 0 , UnityEngine.Random.Range(0, 360) , 0);


					GameObject obj = (GameObject)GameObject.Instantiate (prefab, pos, Quaternion.Euler(prefab.transform.eulerAngles + randomRotation));
					obj.transform.position = pos;
					obj.transform.parent = tile.transform;
				}
			}

			yield return null;
		}

	}
}
