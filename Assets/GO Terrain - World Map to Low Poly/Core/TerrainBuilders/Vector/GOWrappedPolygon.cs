using UnityEngine;
using System.Collections.Generic;

public class GOWrappedPolygon {
	
	public List<Vector3> vertices = new List<Vector3>(9);

	public GOWrappedPolygon(params Vector3[] vts) {
		vertices.AddRange( vts );
	}

	public static GOWrappedPolygon ClipPolygon (GOWrappedPolygon polygon, Plane plane) {
		
		bool[] positive = new bool[9];
		int positiveCount = 0;

		for(int i = 0; i < polygon.vertices.Count; i++) {
			positive[i] = !plane.GetSide( polygon.vertices[i] );
			if(positive[i]) positiveCount++;
		}
		
		if(positiveCount == 0) return null; // Fully behind the plane
		if(positiveCount == polygon.vertices.Count) return polygon; // Fully in front of the plane

		GOWrappedPolygon tempPolygon = new GOWrappedPolygon();

		for(int i = 0; i < polygon.vertices.Count; i++) {
			int next = i + 1;
			 next %= polygon.vertices.Count;

			if( positive[i] ) {
				tempPolygon.vertices.Add( polygon.vertices[i] ); 
			}

			if( positive[i] != positive[next] ) {
				Vector3 v1 = polygon.vertices[next];
				Vector3 v2 = polygon.vertices[i];
				
				Vector3 v = LineCast(plane, v1, v2);
				tempPolygon.vertices.Add( v );
			}
		}
			
		return tempPolygon;
	}

	public static GOWrappedPolygon WrapPolygon (GOWrappedPolygon polygon, Vector3[] shape, GameObject objectToWrap) {


		bool[] positive = new bool[polygon.vertices.Count];
		int positiveCount = 0;
//		Vector3 offsetY = new Vector3 (0, 0.05f, 0);

		for(int i = 0; i < polygon.vertices.Count; i++) {
			positive [i] = ContainsPoint2D (shape,polygon.vertices[i]);
			if(positive[i]) positiveCount++;
		}

		if (positiveCount == 0) {
			return null; // Fully outside the shape
		} 

		return polygon; // Return all polygon that are partiallio inside the shape

	}

	private static Vector3 LineCast(Plane plane, Vector3 a, Vector3 b) {
		float dis;
		Ray ray = new Ray(a, b-a);
		plane.Raycast( ray, out dis );
		return ray.GetPoint(dis);
	}


	public static bool ContainsPoint2D (Vector3[] polyPoints, Vector3 p) { 
		var j = polyPoints.Length-1; 
		var inside = false; 
		for (int i = 0; i < polyPoints.Length; j = i++) { 
			
			if (((polyPoints [i].z <= p.z && p.z < polyPoints [j].z) || (polyPoints [j].z <= p.z && p.z < polyPoints [i].z)) &&
			    (p.x < (polyPoints [j].x - polyPoints [i].x) * (p.z - polyPoints [i].z) / (polyPoints [j].z - polyPoints [i].z) + polyPoints [i].x)) {
				inside = !inside;
			}
				 
		} 

	//	Debug.Log ("Point is inside: " + (inside ? "true" : "false"));

		return inside; 
	}

	static Vector3 ShapeIntersection (Vector3[] shape, Vector3 lineStart, Vector3 lineEnd) { 

		for (int i = 0; i < shape.Length; i++) {
		
			Vector3 v1 = shape [i];
			Vector3 v2;

			if (i + 1 == shape.Length) {
				v1 = shape [0];
				v2 = shape [shape.Length - 1];
			} else {
				v2 = shape [i+1];
			}

			Vector3 intersection;
			LineLineIntersection2D (out intersection, lineStart, v1, lineEnd, v2);

			if (intersection != Vector3.zero) {
			//	Debug.Log ("Returning INTERSECTION! " + intersection);
				return intersection;
			}
		}

		return lineStart;
	}

	public static bool LineLineIntersection(out Vector3 intersection, Vector3 linePoint1, Vector3 lineVec1, Vector3 linePoint2, Vector3 lineVec2){

		Vector3 lineVec3 = linePoint2 - linePoint1;
		Vector3 crossVec1and2 = Vector3.Cross(lineVec1, lineVec2);
		Vector3 crossVec3and2 = Vector3.Cross(lineVec3, lineVec2);

		float planarFactor = Vector3.Dot(lineVec3, crossVec1and2);

		//is coplanar, and not parrallel
		if(Mathf.Abs(planarFactor) < 0.0001f && crossVec1and2.sqrMagnitude > 0.0001f)
		{
			float s = Vector3.Dot(crossVec3and2, crossVec1and2) / crossVec1and2.sqrMagnitude;
			intersection = linePoint1 + (lineVec1 * s);
			return true;
		}
		else
		{
			intersection = Vector3.zero;
			return false;
		}
	}

	public static bool LineLineIntersection2D(out Vector3 intersection, Vector3 linePoint1, Vector3 lineVec1, Vector3 linePoint2, Vector3 lineVec2){

//		float y = Vector3.Lerp(linePoint1, linePoint2,0.5f).y;

		linePoint1.y = 0;
		lineVec1.y = 0;
		linePoint2.y = 0;
		lineVec2.y = 0;

	
		Vector3 lineVec3 = linePoint2 - linePoint1;
		Vector3 crossVec1and2 = Vector3.Cross(lineVec1, lineVec2);
		Vector3 crossVec3and2 = Vector3.Cross(lineVec3, lineVec2);

		float planarFactor = Vector3.Dot(lineVec3, crossVec1and2);

		//is coplanar, and not parrallel
		if(Mathf.Abs(planarFactor) < 0.0001f && crossVec1and2.sqrMagnitude > 0.0001f)
		{
			float s = Vector3.Dot(crossVec3and2, crossVec1and2) / crossVec1and2.sqrMagnitude;
			intersection = linePoint1 + (lineVec1 * s);
//			intersection.y = y;
			return true;
		}
		else
		{
			intersection = Vector3.zero;
			return false;
		}
	}

	static bool CreateDebugSphere ( Vector3 position , string name, float scale, GameObject parent){
		
		GameObject test2 = GameObject.CreatePrimitive (PrimitiveType.Sphere);
		test2.name = name;
		test2.transform.position = Vector3.Scale (position, new Vector3 (1, 0, 1));
		//test2.transform.parent = parent.transform;
		test2.transform.localScale = new Vector3 (scale,scale,scale);
		return true;

	}

	static bool VisualizeGODecalPolygon( GOWrappedPolygon decal, bool flat, Color color, float offset ){
		
		for (int i = 0; i < decal.vertices.Count; i++){
			Vector3 v1 = decal.vertices [i];
			Vector3 v2;
			Vector3 vOffset = new Vector3 (0, offset, 0);
			if (i + 1 == decal.vertices.Count) {
				v1 = decal.vertices [0];
				v2 = decal.vertices [decal.vertices.Count - 1];
			} else {
				v2 =  decal.vertices [i+1];
			}
			if (flat) {
				Debug.DrawLine (Vector3.Scale (v1, new Vector3 (1, 0, 1))+vOffset, Vector3.Scale (v2, new Vector3 (1, 0, 1))+vOffset, color, 1000, false);
			} else {
				Debug.DrawLine (v1, v2, Color.magenta, 1000, false);

			}
		}
		return true;
	
	}
	
}
