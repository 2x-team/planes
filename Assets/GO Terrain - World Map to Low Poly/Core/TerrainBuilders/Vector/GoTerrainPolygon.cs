﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.IO;
using GoShared;

namespace GoTerrain {

	public class GOTerrainPolygon
	{
		public GOTerrainTile tile;
		IList subject;
		Material mat;
		public Mesh mesh;
		public Vector3[] shape;

		public GOTerrainPolygon(IList _subject, List<object> _clips, GOTerrainTile _tile, Material _mat) // To be moved in GoTerrainLayer
		{
			subject = _subject;
//			clips = _clips;
			tile = _tile;
			mat = _mat;
		}

		public GameObject createTerrainObject (GameObject go, float pushDistance) {

			MeshFilter filter = go.AddComponent<MeshFilter>();

			if (mat) {
				MeshRenderer renderer = go.AddComponent<MeshRenderer> ();
				renderer.sharedMaterial = mat;
				renderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
			}

			shape = CoordsToVerts (subject,go);
			GOTerrainMeshWrapper.WrapMeshToObject (go, shape, tile.gameObject, 360);
			GOTerrainMeshWrapper.Push(0);
			mesh = GOTerrainMeshWrapper.CreateMesh();

			if (mesh != null) {
				mesh.name = "Wrapped Mesh";
				filter.mesh = mesh;
			} else {
//				Debug.LogWarning ("Mesh generated is null!  "+ tile.gameObject.name);
				return go;
			}

//			SimplePolygon polygon = go.AddComponent<SimplePolygon> ();
//			Mesh mesh = polygon.load(shape.ToList());
//			mesh.name = "Test mesh";
//			filter.mesh = mesh;

			//FIX UV
			Vector2[] uvs = new Vector2[mesh.vertices.Length];
			for (int i=0; i < uvs.Length; i++) {
				uvs[i] = new Vector2(mesh.vertices[i].x, mesh.vertices[i].z);
			}
			mesh.uv = uvs;

			//Add mesh collider
			MeshCollider collider = go.AddComponent<MeshCollider> ();
			collider.sharedMesh = mesh;


			return go;
		}

		/*
		public GameObject createVoronoiTerrain (GameObject go) {

			MeshFilter filter = go.AddComponent<MeshFilter>();

			if (mat)
				go.AddComponent<MeshRenderer> ().sharedMaterial = mat;

			//Polygon shape
			Vector3[] shape = CoordsToVerts (subject,go);

			//Below terrain portion
			List<Vector3> filteredGrid = new List<Vector3> ();
			foreach (Vector3 vert in tile.gameObject.GetComponent<MeshFilter>().mesh.vertices) {

			
				if (GODecalPolygon.ContainsPoint2D(shape.ToArray(),go.transform.TransformPoint(vert))) {
					filteredGrid.Add (vert);
					//				Debug.Log (vert.y);
					//				Debug.DrawLine (newvert, newvert + Vector3.up*10, Color.yellow,1000);
				}
			}
			if (filteredGrid.Count == 0) {
				Debug.Log ("Tile" + tile.name);
			}

			Mesh mesh = GOVoronoi.TriangulateVoronoyMesh(shape.ToList(),filteredGrid);

			List <Vector3> vertAlt = new List<Vector3>();
			foreach (Vector3 v in mesh.vertices) {
				var newv = go.transform.TransformPoint (v);
				vertAlt.Add(new Vector3 (v.x,GOTerrain.RaycastAltitudeForVector (newv),v.z));
			}
			mesh.vertices = vertAlt.ToArray();

			if (mesh != null) {
				mesh.name = "Wrapped Mesh";
				filter.mesh = mesh;
			} else {
				Debug.LogWarning ("Mesh generated is null!  "+ tile.gameObject.name);
				return go;
			}

//			SimplePolygon polygon = go.AddComponent<SimplePolygon> ();
//			Mesh mesh = polygon.load(shape.ToList());
//			mesh.name = "Test mesh";
//			filter.mesh = mesh;

			//FIX UV
			Vector2[] uvs = new Vector2[mesh.vertices.Length];
			for (int i=0; i < uvs.Length; i++) {
				uvs[i] = new Vector2(mesh.vertices[i].x, mesh.vertices[i].z);
			}
			mesh.uv = uvs;

			//Add mesh collider
			MeshCollider collider = go.AddComponent<MeshCollider> ();
			collider.sharedMesh = mesh;

			go.transform.position = go.transform.position + new Vector3(0,1,0);


			return go;
		}
		*/

		public Vector3[] CoordsToVerts (IList polygon, GameObject go) {

			var l = new List<Vector3>();
			for (int i = 0; i < polygon.Count - 1; i++)
			{
				IList c = (IList)polygon[i];
				Coordinates coords = new Coordinates ((double)c[1], (double)c[0],0);
				Vector3 p = coords.convertCoordinateToVector();

				p = go.transform.InverseTransformPoint (p);

				l.Add (p);
			}
			return l.ToArray();
		}

		public IEnumerator SpawnPrefabsInMesh (GameObject[] prefabs, GameObject parent, int density) {
		
			if (!mesh)
				yield break;
			
			int rate = 100 / density;


			foreach (Vector3 vertex in mesh.vertices) {

				try {
				int spawn = UnityEngine.Random.Range (0, rate);
				if (spawn != 0)
					continue;
				
				int n = UnityEngine.Random.Range (0, prefabs.Length);
				Vector3 pos = vertex;

				if(GOTerrain.IsPositionAboveWater(pos))
					continue;

				pos.y += prefabs [n].transform.position.y;
	

				var rotation = prefabs [n].transform.eulerAngles;
				var randomRotation =  new Vector3( 0 , UnityEngine.Random.Range(0, 360) , 0);

				GameObject obj =  (GameObject)GameObject.Instantiate (prefabs[n], pos, Quaternion.Euler(rotation+randomRotation));
				obj.transform.parent = parent.transform;

				} catch {
				}

				if (Application.isPlaying)
					yield return null;
				
			}


			yield return null;
		}
			
		public Vector3 RandomPositionInMesh(Mesh mesh, GameObject gameObject){

			Bounds bounds = mesh.bounds;

			float minX = bounds.size.x * 0.5f;
			float minZ = bounds.size.z * 0.5f;

			Vector3 newVec = new Vector3(UnityEngine.Random.Range (minX, -minX),
				gameObject.transform.position.y,
				UnityEngine.Random.Range (minZ, -minZ));
			return newVec;
		}

	}
}
