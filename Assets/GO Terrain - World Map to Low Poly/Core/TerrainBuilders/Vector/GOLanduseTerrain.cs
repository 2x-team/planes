﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections.Generic;
using GoShared;
using System;





namespace GoTerrain {
	
	public class GOLanduseTerrain : MonoBehaviour {

		public enum GOLanduseKind {

			baseKind,
			aerodrome,
			allotments,
			amusement_ride,
			animal,
			apron,
			aquarium,
			artwork,
			attraction,
			aviary,
			battlefield,
			beach,
			breakwater,
			bridge,
			camp_site,
			caravan_site,
			carousel,
			cemetery,
			cinema,
			city_wall,
			college,
			commercial,
			common,
			cutline,
			dam, //polygon, line,
			dike,
			dog_park,
			enclosure,
			farm,
			farmland,
			farmyard,
			fence,
			footway,
			forest,
			fort,
			fuel,
			garden,
			gate,
			generator,
			glacier,
			golf_course,
			grass,
			grave_yard,
			groyne,
			hanami,
			hospital,
			industrial,
			land,
			library,
			maze,
			meadow,
			military,
			national_park,
			nature_reserve,
			natural_forest,// - See planned bug fix in #1096.
			natural_park,// - See planned bug fix in #1096.
			natural_wood,// - See planned bug fix in #1096.
			park,
			parking,
			pedestrian,
			petting_zoo,
			picnic_site,
			pier,
			pitch,
			place_of_worship,
			plant,
			playground,
			prison,
			protected_area,
			quarry,
			railway,
			recreation_ground,
			recreation_track,
			residential,
			resort,
			rest_area,
			retail,
			retaining_wall,
			rock,
			roller_coaster,
			runway,
			rural,
			school,
			scree,
			scrub,
			service_area,
			snow_fence,
			sports_centre,
			stadium,
			stone,
			substation,
			summer_toboggan,
			taxiway,
			theatre,
			theme_park,
			tower,
			trail_riding_station,
			tree_row,
			university,
			urban_area,
			urban,
			village_green,
			wastewater_plant,
			water_park,
			water_slide,
			water_works,
			wetland,
			wilderness_hut,
			wildlife_park,
			winery,
			winter_sports,
			wood,
			works,
			zoo
		};

		public static GameObject BuildLanduseTerrain (GameObject parent, string kind_, IList _subject, List<object> _clips, GOTerrainTile _tile, float distance, IDictionary properties) {

			GOLanduseKind kind = LanduseKindToEnum (kind_);

			GOTerrainVectorLanduseOptions baseKind = null; //base landuse options set
			GOTerrainVectorLanduseOptions options = null; //current landuse options set

			foreach (GOTerrainVectorLanduseOptions opt in _tile.map.goTerrainOptions.vectorOptions.landuseOptions) {
				if (opt.kind == GOLanduseKind.baseKind)
					baseKind = opt;

				if (opt.kind == kind) {
					options = opt;
					break;
				}
			}

			if (options == null && baseKind == null)
				return null;

			if (options != null && options.dontShow)
				return null;

			if (options == null && baseKind != null) {
				options = baseKind;
			}

			if (!options.material && options.prefabs.Count () == 0)
				return null;

			GameObject feature = null;

			GOTerrainPolygon poly = new GOTerrainPolygon (_subject, _clips, _tile, options.material);
			feature = new GameObject();
			feature.transform.parent = parent.transform;
			feature = poly.createTerrainObject (feature,distance);

			if (options.kind == GOLanduseKind.bridge) {

				GOLanduseTerrain.BuildABridge (_subject, options, _tile, feature,poly, properties);

			} else {

				if (options.prefabs.Count () > 0 && options.density != 0) {
					GORoutine.start( poly.SpawnPrefabsInMesh (options.prefabs,feature,options.density),_tile);
				}
			}

			return feature;

		}			

		public static string LanduseEnumToString (GOLanduseKind kind) {
			return kind.ToString ();
		}

		public static GOLanduseKind LanduseKindToEnum(string kind) {
			try {
			GOLanduseKind parsed_enum = (GOLanduseKind)System.Enum.Parse( typeof( GOLanduseKind ), kind );
			return parsed_enum;
			} catch {
				return GOLanduseKind.baseKind;
			}
		}

		#region bridge

		private static void BuildABridge(IList _subject, GOTerrainVectorLanduseOptions options, GOTerrainTile _tile, GameObject feature, GOTerrainPolygon poly, IDictionary properties) {

			var id = properties ["id"];
//			feature.GetComponent<MeshRenderer> ().enabled = false;

			//Check if bridge already exist
			GameObject b = GameObject.Find ("bridge-"+id);
			if (b) 
				return;

			if (poly.mesh == null || poly.mesh.bounds.extents == Vector3.zero)
				return;

			List <Vector3> shape = poly.shape.ToList ();
			Vector3 center = shape.Aggregate((acc, cur) => acc + cur) / shape.Count;
			center.y = GOTerrain.RaycastAltitudeForVector (center);

			int n = UnityEngine.Random.Range (0, options.prefabs.Length-1);
			var rotation = options.prefabs[n].transform.eulerAngles;

			//Rotation
			float greaterDist = 0;
			Vector3 pointA = Vector3.zero;
			Vector3 pointB = Vector3.zero;
			foreach (Vector3 pa in shape) {
				foreach (Vector3 pb in shape) {
					float dist = Vector3.Distance (pa, pb);
					if (dist > greaterDist) {
						greaterDist = dist;
						pointA = pa;
						pointB = pb;
					}
				}
			}
			Vector3 targetDir = pointB-pointA;
			Quaternion finalRotation = Quaternion.LookRotation (targetDir);
			rotation.y = finalRotation.eulerAngles.y + 90;

//			GOTerrainBuilder.CreateDebugPosition (pointA, Color.green, 300);
//			GOTerrainBuilder.CreateDebugPosition (pointB, Color.blue
//				, 300);

			GameObject bridge = (GameObject)GameObject.Instantiate (options.prefabs[n], center, Quaternion.Euler(rotation));
			bridge.transform.parent = feature.transform;
			bridge.name = "bridge-" + id;

			Mesh prefabMesh = bridge.GetComponent<MeshFilter> ().sharedMesh;

			//Bridge lenght
			float prefabLenght = Mathf.Max (prefabMesh.bounds.extents.x,prefabMesh.bounds.extents.z);
			float bridgeLenght = Mathf.Max (poly.mesh.bounds.extents.x, poly.mesh.bounds.extents.z);
			float factor = bridgeLenght/prefabLenght;
			bridge.transform.localScale = Vector3.one * factor;

			//heightFix
			float prefabHeight = (prefabMesh.bounds.extents.y/2)*bridge.transform.localScale.y;
			prefabHeight += options.prefabs[n].transform.position.y;
			bridge.transform.position += new Vector3(0,prefabHeight,0);


		}
		#endregion
	}
}
