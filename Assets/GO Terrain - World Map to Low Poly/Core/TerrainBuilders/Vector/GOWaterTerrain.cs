﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections.Generic;


namespace GoTerrain {
	
	public class GOWaterTerrain : MonoBehaviour {

		public enum GOWaterKind {
			
			baseMaterial,
			basin, //polygon
				bay, //point, intended for label placement only
				canal, //line
				ditch, //line
			dock,  //polygon
				drain, //line
				fjord, //point, intended for label placement only
			lake, //polygon
			ocean, //polygon + point, intended for label placement only
			playa, //polygon
				river, //line
			riverbank, //polygon
				sea, //point, intended for label placement only
				stream, //line
				strait, //point, intended for label placement only
			swimming_pool, //polygon
			water //polygon

		};

		public static GameObject BuildWaterTerrain (GameObject parent, string kind_, IList _subject, List<object> _clips, GOTerrainTile _tile) {

			GOWaterKind kind = WaterKindToEnum (kind_);

			if (kind == GOWaterKind.ocean) {

				if (parent.transform.FindChild ("ocean") != null)
					return null;

				GameObject sea = CreateSea (_tile, _tile.map.goTerrainOptions.vectorOptions.seaMaterial);
				sea.transform.parent = parent.transform;

//				sea.transform.position = new Vector3 (120, 0, 842);
				return sea;

			} else {
				
				GOTerrainVectorWaterOptions baseKind = null;
				GOTerrainVectorWaterOptions currentKind = null;

				foreach (GOTerrainVectorWaterOptions opt in _tile.map.goTerrainOptions.vectorOptions.waterOptions) {
					if (opt.kind == GOWaterKind.baseMaterial)
						baseKind = opt;

					if (opt.kind == kind) {
						currentKind = opt;
						break;
					}
				}

				if (currentKind == null && baseKind == null)
					return null;

				if (currentKind != null && currentKind.dontShow)
					return null;

				if (currentKind == null && baseKind != null) {
					currentKind = baseKind;
				}

				if (currentKind == null && baseKind != null) {
					currentKind = baseKind;
				}

				GameObject feature = new GameObject();

				GOTerrainPolygon poly = new GOTerrainPolygon (_subject, _clips, _tile, currentKind.material);
				feature.transform.parent = parent.transform;
				feature = poly.createTerrainObject (feature,1.0f);

				return feature;
			}


		}

		public static GameObject CreateSea (GOTerrainTile tile,  Material mat) {
		
			GameObject sea = new GameObject ("ocean");

			MeshFilter filter = sea.AddComponent<MeshFilter>();
			MeshRenderer renderer = sea.AddComponent<MeshRenderer>();
			renderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;

			Poly2Mesh.Polygon poly = new Poly2Mesh.Polygon();
			poly.outside = tile.tileCenter.tileVertices (tile.map.zoomLevel);
			Mesh mesh = Poly2Mesh.CreateMesh (poly);

			Vector2[] uvs = new Vector2[mesh.vertices.Length];
			for (int i=0; i < uvs.Length; i++) {
				uvs[i] = new Vector2(mesh.vertices[i].x, mesh.vertices[i].z);
			}
			mesh.uv = uvs;

			filter.mesh = mesh;
			renderer.material = mat;

			return sea;
		}

		public static string WaterEnumToString (GOWaterKind kind) {
			return kind.ToString ();
		}

		public static GOWaterKind WaterKindToEnum(string kind) {
			try {
			GOWaterKind parsed_enum = (GOWaterKind)System.Enum.Parse( typeof( GOWaterKind ), kind );
			return parsed_enum;
			} catch {
				return GOWaterKind.baseMaterial;
			}
		}
	}
}

