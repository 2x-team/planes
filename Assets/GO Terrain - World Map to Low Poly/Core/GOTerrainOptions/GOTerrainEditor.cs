﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace GoTerrain {

	#if UNITY_EDITOR

	[CustomEditor(typeof(GOTerrainEditor))]
	public class GOTerrainInspector : Editor
	{
		public override void OnInspectorGUI()
		{
			DrawDefaultInspector();

			GUIStyle style = new GUIStyle ();
			style.fontSize = 12;
			style.normal.textColor = Color.white;
			GUILayout.Space(10);

			GOTerrainEditor editor = (GOTerrainEditor)target;
			GUILayout.Label ("Use this while the application is Not Running!",style);
			GUILayout.Space(20);

			GUILayout.Label ("This script allows you to load the terrain directly\nin the scene.\n" +
				"In this way you can edit it, save it and it will\nbe available offline.");
			GUILayout.Space(20);
			EditorGUILayout.HelpBox ("It might take some time depending on how\nbig is the tile buffer set on GOTerrain component.",MessageType.Info);
			if(GUILayout.Button("Load Terrain in Editor"))
			{
				editor.LoadInsideEditor();
			}
			GUILayout.Space(20);
			EditorGUILayout.HelpBox ("This destroys everything in the terrain hierarchy,\nuse this before loading another terrain inside the editor.",MessageType.Info);
			if(GUILayout.Button("Destroy Terrain in Editor"))
			{
				editor.DestroyCurrentMap();
			}

			//			if(GUILayout.Button("Test editor request"))
			//			{
			//				editor.TestWWWInEditor();
			//			}
		}
	}
	#endif


	public class GOTerrainEditor : MonoBehaviour 
	{

		public void LoadInsideEditor () {

			GOTerrain map = GetComponent<GOTerrain> ();
			if (map == null) {
				Debug.LogError ("[GOTerrain Editor] GOTerrain script not found");
				return;
			}

			map.BuildInsideEditor ();

		}

		public void DestroyCurrentMap () {

			GOTerrain map = GetComponent<GOTerrain> ();
			if (map == null) {
				Debug.LogError ("[GOTerrain Editor] GOTerrain script not found");
				return;
			}

			while (map.transform.childCount > 0) {
				foreach (Transform child in map.transform) {
					GameObject.DestroyImmediate (child.gameObject);
				}
			}

			#if GOLINK
			while (map.goMap.transform.childCount > 0) {
				foreach (Transform child in map.goMap.transform) {
					GameObject.DestroyImmediate (child.gameObject);
				}
			}
			#endif
		}

		public void TestWWWInEditor() {

			GOTerrain map = GetComponent<GOTerrain> ();
			if (map == null) {
				Debug.LogError ("[GOTerrain Editor] GOTerrain script not found! GOTerrainEditor and GOTerrain have to be attached to the same GameObject");
				return;
			}
			map.TestEditorWWW ();
		}

		IEnumerator ClearConsole()
		{
			// wait until console visible
			while(!Debug.developerConsoleVisible)
			{
				yield return null;
			}
			yield return null; // this is required to wait for an additional frame, without this clearing doesn't work (at least for me)
			Debug.ClearDeveloperConsole();
		}

	}
}

