using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GoShared;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace GoTerrain {
	
	[ExecuteInEditMode]
	[System.Serializable]

	public class GOTerrainOptions {

		public APIprovider elevationAPI = APIprovider.Mapzen;
		public enum APIprovider  {

			Mapzen,
			Mapbox	
		}

		[HideInInspector]
		public bool flattenTerrain = false;
		[HideInInspector]
		public bool addDebugMarkers = false;

		[Range(0.1f, 10)]
		public float altitudeMultiplier = 1.0f;

		[Range(4, 254)]
		public int resolution = 50;

		public TerrainGenerationMode generationMode = TerrainGenerationMode.GradientColor;
		public enum TerrainGenerationMode  {
			GradientColor,
			Vector,
			TerrainImagery
		}
			
		[HideInInspector]
		public bool loadEnvironment = true;

		public bool useNormals = true;

		public GOTerrainGradientOptions gradientOptions;
		public GOTerrainImageryOptions imageryOptions;
		public GOTerrainVectorOptions vectorOptions;

		[Header("Required materials, edit at your own risk.")]
		public GOTerrainMaterialOptions baseMaterials;
	}


	[ExecuteInEditMode]
	[System.Serializable]
	public class GOTerrainGradientOptions {

		public Gradient gradient;	
		public bool useLowPoly = true;

		[Range(-11000, 2000)]
		public int startAltitude = 0;

		[Range(-1000, 8900)]
		public int endAltitude= 4000;

		[Header("Numbers of colors used in gradient")]	 
		[Range(4, 100)]
		public int posterizationLevels = 100;
	}



	[ExecuteInEditMode]
	[System.Serializable]
	public class GOTerrainImageryOptions {

		public TerrainImageryType imageService = GOTerrainImageryOptions.TerrainImageryType.Terrain;
		public enum TerrainImageryType  {

			Satellite,
			Watercolor,
			Terrain,
//			Toner
//			Physical
		}

		public TerrainImageryMode imageMode = GOTerrainImageryOptions.TerrainImageryMode.Texture;
		public enum TerrainImageryMode  {

//			Color,
			Texture,
			LowPoly_VertexColor,
			LowPoly_Texture
		}
	}

	[ExecuteInEditMode]
	[System.Serializable]
	public class GOTerrainMaterialOptions {

		public Material gradientMaterial;
		public Material lowPolyMaterial;
		public Material terrainImagegeryMaterial;
	
	}

	[ExecuteInEditMode]
	[System.Serializable]
	public class GOTerrainVectorOptions {

		[Header("Base material for terrain")]
		public Material earthMaterial;
		[Header("Base material for seas/oceans")]
		public Material seaMaterial;

		[Header("Base prefabs for clouds")]	 
		public GameObject[] cloudPrefabs;

		[Header("Custom settings for water kinds")]
		public GOTerrainVectorWaterOptions[] waterOptions ;

		[Header("Custom settings for landuse kinds")]
		public List<GOTerrainVectorLanduseOptions> landuseOptions;
	}

	[ExecuteInEditMode]
	[System.Serializable]
	public class GOTerrainVectorWaterOptions {

		public GOWaterTerrain.GOWaterKind kind;
		public bool dontShow = false;
		public Material material;

	}

	[ExecuteInEditMode]
	[System.Serializable]
	public class GOTerrainVectorLanduseOptions {

		public GOLanduseTerrain.GOLanduseKind kind;
		public bool dontShow = false;
		public Material material;
		public GameObject[] prefabs;
		[Range(0, 100)]
		public int density;

	}

//	#if UNITY_EDITOR
//
//	[CustomEditor(typeof(GOTerrainVectorWaterOptions))]
//	public class WaterOptionsEditor : Editor
//	{
//		public override void OnInspectorGUI()
//		{
//			DrawDefaultInspector();
//
////			GOTerrainVectorWaterOptions myScript = (GOTerrainVectorWaterOptions)target;
//			if(GUILayout.Button("Remove"))
//			{
////				myScript.Remove();
//			}
//		}
//	}
//	#endif
}