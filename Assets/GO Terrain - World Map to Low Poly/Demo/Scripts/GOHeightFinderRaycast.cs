﻿using UnityEngine;
using System.Collections;

public class GOHeightFinderRaycast : MonoBehaviour {

	void Update() {
		if (Input.GetMouseButtonDown(0)) {
			RaycastHit hit;
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			if (Physics.Raycast (ray, out hit)) {
				Debug.Log ("Height in this point is: "+hit.point.y + "  "+hit.transform.gameObject.name);
			}
		}
	}
}
