﻿using UnityEngine;
using System.Collections;
using GoTerrain;
using GoShared;

namespace GoShared {

	public class MoveAvatarGPS : MonoBehaviour {

		public LocationManager locationManager;
		public GOTerrain goTerrain;
		public GameObject avatarFigure;

		// Use this for initialization
		void Start () {

			locationManager.onLocationChanged += OnLocationChanged;
			goTerrain.didLoadTile.AddListener (DidLoadTile);

		}

		public void DidLoadTile (GOTerrainTile tile) {

			if (tile.tileCenter.isEqualToCoordinate(locationManager.currentLocation.tileCenter(goTerrain.zoomLevel))) {

				Vector3 currentPosition = locationManager.currentLocation.convertCoordinateToVector ();
				currentPosition.y = GOTerrain.RaycastAltitudeForVector (currentPosition);
				//currentPosition.y = goTerrain.FindAltitudeForVector (currentPosition);
	//			Debug.Log ("[MOVE AVATAR GPS] Did load tile: " + tile.tileCenter.convertCoordinateToVector()+"position: " +currentPosition + " Height: " +GOTerrain.RaycastAltitudeForVector (currentPosition));
				StartCoroutine(move (avatarFigure.transform.position, currentPosition, 0, 0.5f, false));
			} 
		}

		void OnLocationChanged (Coordinates currentLocation) {

			Vector3 lastPosition = transform.position;

			//Position
			Vector3 currentPosition = currentLocation.convertCoordinateToVector ();

			currentPosition.y = GOTerrain.RaycastAltitudeForVector (currentPosition);
			//currentPosition.y = goTerrain.FindAltitudeForVector (currentPosition);

			if (lastPosition == Vector3.zero) {
				lastPosition = currentPosition;
			}

			moveAvatar (lastPosition,currentPosition,true);

		}

		void moveAvatar (Vector3 lastPosition, Vector3 currentPosition,bool rotateBody) {

			StartCoroutine (move (lastPosition,currentPosition,0,0.5f,rotateBody));
		}

		private IEnumerator move(Vector3 lastPosition, Vector3 currentPosition, float afterDelay,float time,bool rotateBody) {

			float elapsedTime = 0;
			Vector3 targetDir = currentPosition-lastPosition;

			if (afterDelay > 0) {
				Debug.Log ("Yielding");
				yield return new WaitForSeconds(afterDelay);
			}

			if (!rotateBody)
				targetDir = Vector3.zero;

			Quaternion finalRotation = Quaternion.LookRotation (targetDir);

			while (elapsedTime < time)
			{
				transform.position = Vector3.Lerp(lastPosition, currentPosition, (elapsedTime / time));
				avatarFigure.transform.rotation = Quaternion.Lerp(avatarFigure.transform.rotation, finalRotation,(elapsedTime / time));

				elapsedTime += Time.deltaTime;
				yield return new WaitForEndOfFrame();
			}

			transform.localPosition = currentPosition;
		}


	}

}