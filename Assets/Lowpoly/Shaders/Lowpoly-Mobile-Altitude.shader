Shader "LowPoly/Mobile-Altitude" {
Properties {
      _MainTex ("Texture", 2D) = "white" {}
	_Color("Color", Color) = (1,1,1,1)
}
SubShader {
	Tags { "RenderType"="Opaque" }
	LOD 150

CGPROGRAM
#pragma surface surf SimpleSpecular vertex:vert

sampler2D _MainTex;
uniform float4 _Color;

 struct Input {
   float3 localPos;
    float2 uv_MainTex;
 };

     half4 LightingSimpleSpecular (SurfaceOutput s, half3 lightDir, half3 viewDir, half atten) {
        half3 h = normalize (lightDir + viewDir);

        half diff = max (0, dot (s.Normal, lightDir));

        float nh = max (0, dot (s.Normal, h));
        float spec = pow (nh, 48.0);

        half4 c;
        c.rgb = (s.Albedo * _LightColor0.rgb * diff + _LightColor0.rgb * spec) * atten;
        c.a = s.Alpha;
        return c;
    }


  void vert (inout appdata_full v, out Input o) {
   UNITY_INITIALIZE_OUTPUT(Input,o);
   o.localPos = v.vertex.xyz;
 }

void surf (Input IN, inout SurfaceOutput o) {

	float3 derivedNormal = cross( normalize(ddx( IN.localPos.xyz)), normalize(ddy(IN.localPos.xyz)));
	float derivedAbs =  (abs(derivedNormal.x) +abs(derivedNormal.y)+abs(derivedNormal.z))/3.0f ;
	float magnitudeNormal = (( 1 - 0.35))*(derivedAbs -1)+1;

	o.Albedo =  tex2D (_MainTex, IN.uv_MainTex).rgb *_Color * magnitudeNormal ;
}
ENDCG
}

Fallback "Mobile/Diffuse"
}
