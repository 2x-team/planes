﻿// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'


Shader "LowPoly/MobileWater" {
Properties {
    _Color ("Main Color", Color) = (1,1,1,1)
    _MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
		//WATER
		_Speed ("Speed", Range (0.01, 200)) = 1.3
		_Offset ("Offset", Range (-5, 5)) = 0
		_Ampitude ("Ampitude", Range (0.0, 10)) = 0.0
		_Alpha ("Alpha", Range(0.1, 1.0)) = 1.0

}
 
SubShader {
    Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
    LOD 200
 
CGPROGRAM
#pragma surface surf Lambert vertex:vert alpha
 
sampler2D _MainTex;
fixed4 _Color;
 		//WATER
	uniform float _Speed;
	uniform float _Ampitude;
	uniform float _Alpha;
	uniform float _Offset;

struct Input {
	   float3 localPos;
	   float4 color: Color;
	 };

	 float rand(float3 co){
					return frac(sin(dot(co.xyz ,float3(654.4324,43.321,32.432))) * 25687.5453);
		}

	  void vert (inout appdata_full v, out Input o) {

	   UNITY_INITIALIZE_OUTPUT(Input,o);

	   	float3 offset = float3(0,0,0);
		offset.y += (sin(_Time*rand(v.vertex.xzz)*_Speed)+1.0)*_Ampitude + _Offset;
		v.vertex.xyz += mul((float3x3)unity_WorldToObject, offset);

	    o.localPos = v.vertex.xyz;

	 }
	void surf (Input IN, inout SurfaceOutput o) {

		float3 derivedNormal = cross( normalize(ddx( IN.localPos.xyz)), normalize(ddy(IN.localPos.xyz)));
		float derivedAbs =  (abs(derivedNormal.x) +abs(derivedNormal.y)+abs(derivedNormal.z))/3.0f ;
		float magnitudeNormal = (( 1 - 0.35))*(derivedAbs -1)+1;

		o.Albedo =  _Color * magnitudeNormal * IN.color.rgb;
		o.Alpha =  IN.color.a * _Alpha;
	}
ENDCG
}
Fallback "Transparent/VertexLit"
}