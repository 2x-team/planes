﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Global-Mapper" {
Properties {
    _PeakColor ("PeakColor", Color) = (0.8,0.9,0.9,1)   
    _PeakLevel ("PeakLevel", Float) = 300
    _Level3Color ("Level3Color", Color) = (0.75,0.53,0,1)
    _Level3 ("Level3", Float) = 200
    _Level2Color ("Level2Color", Color) = (0.69,0.63,0.31,1)
    _Level2 ("Level2", Float) = 100
    _Level1Color ("Level1Color", Color) = (0.65,0.86,0.63,1)
    _WaterLevel ("WaterLevel", Float) = 0
    _WaterColor ("WaterColor", Color) = (0.37,0.78,0.92,1)
    _Slope ("Slope Fader", Range (0,1)) = 0

}
SubShader {
	Tags { "RenderType"="Opaque" }
	LOD 150

CGPROGRAM
#pragma surface surf SimpleSpecular vertex:vert


    float _PeakLevel;
    float4 _PeakColor;
    float _Level3;
    float4 _Level3Color;
    float _Level2;
    float4 _Level2Color;
    float _Level1;
    float4 _Level1Color;
    float _Slope;
    float _WaterLevel;
    float4 _WaterColor;

 struct Input {
   float3 localPos;
   float3 worldPos;
   float4 colorCalculated;

 };

     half4 LightingSimpleSpecular (SurfaceOutput s, half3 lightDir, half3 viewDir, half atten) {
        half3 h = normalize (lightDir + viewDir);

        half diff = max (0, dot (s.Normal, lightDir));

        float nh = max (0, dot (s.Normal, h));
        float spec = pow (nh, 48.0);

        half4 c;
        c.rgb = (s.Albedo * _LightColor0.rgb * diff + _LightColor0.rgb * spec) * atten;
        c.a = s.Alpha;
        return c;
    }


  void vert (inout appdata_full IN, out Input o) {

   UNITY_INITIALIZE_OUTPUT(Input,o);
   o.localPos = IN.vertex.xyz;
   o.worldPos = mul(unity_ObjectToWorld, IN.vertex).xyz;


          if (o.worldPos.y >= _PeakLevel)
           o.colorCalculated = _PeakColor;
        if (o.worldPos.y <= _PeakLevel)
            o.colorCalculated = lerp(_Level3Color, _PeakColor, (o.worldPos.y - _Level3)/(_PeakLevel - _Level3));
        if (o.worldPos.y <= _Level3)
            o.colorCalculated = lerp(_Level2Color, _Level3Color, (o.worldPos.y - _Level2)/(_Level3 - _Level2));
        if (o.worldPos.y <= _Level2)
            o.colorCalculated = lerp(_Level1Color, _Level2Color, (o.worldPos.y - _WaterLevel)/(_Level2 - _WaterLevel));
        if (o.worldPos.y <= _WaterLevel)
            o.colorCalculated = _WaterColor;


 }

void surf (Input IN, inout SurfaceOutput o) {

	float3 derivedNormal = cross( normalize(ddx( IN.localPos.xyz)), normalize(ddy(IN.localPos.xyz)));
	float derivedAbs =  (abs(derivedNormal.x) +abs(derivedNormal.y)+abs(derivedNormal.z))/3.0f ;
		float magnitudeNormal = (( 1 - 0.35))*(derivedAbs -1)+1;

		o.Albedo =  IN.colorCalculated.rgb *  magnitudeNormal *saturate(magnitudeNormal* magnitudeNormal * IN.colorCalculated.rgb + _Slope); 

	


	o.Alpha =  1.0f;
}
ENDCG
}

Fallback "Mobile/VertexLit"
}
