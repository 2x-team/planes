﻿using UnityEngine;
using System.Collections;

public class Shooter : MonoBehaviour
{
	public GameObject projectile;
	public Transform shotPos;
	public float shotForce = 1000f;

	public float fireRate = 0.5F;
	private float nextFire = 0.0F;
	
	void Update ()
	{

		if (Input.GetKey(KeyCode.Space) && Time.time > nextFire){

			nextFire = Time.time + fireRate;

			GameObject shot = Instantiate(projectile, shotPos.position,Quaternion.identity ) as GameObject;
			//shot.AddForce(shot.transform.forward * shotForce);
			shot.GetComponent<Rigidbody>().velocity = transform.TransformDirection(new Vector3(0, 0,shotForce));
			Destroy(shot, 2.0f);

		}

	


	}



}