﻿using UnityEngine;
using System.Collections;

public class bullet : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	void OnCollisionEnter(Collision collision)
	{
		if (collision.collider.gameObject.tag != "Player")
		{
			Debug.Log ("destroy "+collision.collider.gameObject.name);
			collision.collider.gameObject.SetActive(false);
			Destroy(collision.collider.gameObject);
			gameObject.SetActive(false);
			Destroy(gameObject);

		}        
	}
}
